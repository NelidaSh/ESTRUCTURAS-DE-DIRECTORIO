# Estructura de directorios para proyectos de JavaScript / Node

Si bien la siguiente estructura no es un requisito absoluto ni es impuesta, es una recomendación basada en lo que JavaScript y, en particular, la comunidad de Node en general han estado siguiendo por convención.

## Directorios

* `lib/` es destinado para código que se pueda ejecutar como está.
* `src/` está destinado a un código que necesita ser manipulado antes de que pueda ser utilizado.
* `build/` es para cualquier scripts o tooling(herramienta) necesaria para construir su proyecto.
* `dist/` es para módulos compilados que se pueden usar con otros sistemas.
* `bin/` es para cualquier scripts ejecutable, o binario compilado utilizado con, o creado a partir de su módulo.
* `test/` es para todos los scripts de prueba de su proyecto/módulo.
* `unit/` es un subdirectorio para pruebas unitarias.
* `integration/` es un subdirectorio para pruebas de integración.
* `env/` es para cualquier entorno que se necesita para probar.

## lib & src

La diferencia en el uso de `lib` vs` src` debería ser:

* `lib` si puedes usar `require ()` de node directamente
* `src` si no puede, o el archivo debe ser manipulado antes de su uso.

Si está committing copias de módulos/archivos que son de otros sistemas, se sugiere el uso de `(lib|src)/vendor/(vendor-name)/(project-name)/`.

## build

Si tienes scripts/tools que son necesarios para construir su proyecto, deberían residir en el directorio `build`. Los ejemplos incluyen scripts para obtener datos de origen externo como parte de su proceso de compilación. Otro ejemplo sería usar `build/tasks/` como un directorio para separar tareas en un proyecto.

## dist

Si su proyecto/módulo debe construirse para su uso con otras plataformas (ya sea directamente en el navegador), o en un sistema `AMD` (como `require.js`), entonces estos archivos deben residir bajo el `dist` directorio.

Se recomienda utilizar un formato `(módulo)-(versión).(Plataforma).[Min].js` para los archivos que salen en este directorio. Por ejemplo `foo-0.1.0.browser.min.js` o `foo-0.1.0.amd.js`.

## bin

La carpeta `bin` es para cualquier módulo de sistema que su paquete use y/o genere.

* El resultado compilado `node_gyp` para el código binario de su módulo.
* Binarios de plataforma pre compilados
* <code>[package.json/bin](http://browsenpm.org/package.json#bin)</code> scripts para su módulo.
